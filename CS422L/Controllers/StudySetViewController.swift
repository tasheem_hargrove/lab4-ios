//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Tasheem Hargrove on 2/14/22.
//

import UIKit

class StudySetViewController: UIViewController {
    
    var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var term: UILabel!
    @IBOutlet var exitBtn: UIButton!
    @IBOutlet var missedBtn: UIButton!
    @IBOutlet var skipBtn: UIButton!
    @IBOutlet var correctBtn: UIButton!
    @IBOutlet var cardVerticalContainer: UIStackView!
    var currentIndex = 0
    var missedCollection: [Flashcard] = []
    var correctCollection: [Flashcard] = []
    var frontSideUp = true
    @IBOutlet var missedLabel: UILabel!
    @IBOutlet var correctLabel: UILabel!
    @IBOutlet var completedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        term.text = cards[currentIndex].term
        
        // Clicking the flashcard should reveal the definition
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(flipCard))
        
        cardVerticalContainer.addGestureRecognizer(recognizer)
        
        missedBtn.tintColor = .red
        missedBtn.layer.borderColor = UIColor.red.cgColor
        missedBtn.layer.borderWidth = CGFloat(3)
        
        skipBtn.tintColor = .systemBlue
        skipBtn.layer.borderColor = UIColor.systemBlue.cgColor
        skipBtn.layer.borderWidth = CGFloat(3)
        
        correctBtn.tintColor = .systemGreen
        correctBtn.layer.borderColor = UIColor.systemGreen.cgColor
        correctBtn.layer.borderWidth = CGFloat(3)
        
        missedLabel.text = "Missed: \(missedCollection.count)"
        correctLabel.text = "Correct: \(correctCollection.count)"
        completedLabel.text = "Completed: \(currentIndex) / \(cards.count)"
        
        missedLabel.textColor = .systemRed
        correctLabel.textColor = .systemGreen
    }
    
    @objc func flipCard() {
        if frontSideUp { // Flip card to back
            term.text = cards[currentIndex].definition
            frontSideUp = false
        } else { // Flip to front
            term.text = cards[currentIndex].term
            frontSideUp = true
        }
    }
    
    
    @IBAction func skipCard(_ sender: Any) {
        if currentIndex >= cards.count {
            return
        }
        
        moveToEnd()
        frontSideUp = true
        updateDisplay()
    }
    
    @IBAction func trackMissed(_ sender: Any) {
        if currentIndex >= cards.count {
            return
        }
        
        self.missedCollection.append(cards[currentIndex])
        moveToEnd()
        frontSideUp = true
        updateDisplay()
    }
    
    @IBAction func trackCorrect(_ sender: Any) {
        if currentIndex >= cards.count {
            return
        }
        
        let currentCard = cards[currentIndex]
        var wasMissed = false
        for card in missedCollection {
            // Compare object references with triple equals
            if card === currentCard {
                wasMissed = true
                break;
            }
        }
        
        if !wasMissed {
            self.correctCollection.append(cards[currentIndex])
        }
        self.currentIndex += 1
        
        frontSideUp = true
        updateDisplay()
    }
    
    func moveToEnd() {
        var i = currentIndex
        while i < cards.count - 1 {
            swap(i, i + 1)
            i = i + 1
        }
    }
    
    func swap(_ left: Int, _ right: Int) {
        let temp = cards[left]
        cards[left] = cards[right]
        cards[right] = temp
    }
    
    func updateDisplay() {
        if currentIndex < cards.count {
            self.term.text = self.cards[currentIndex].term
        }
        
        missedLabel.text = "Missed: \(missedCollection.count)"
        correctLabel.text = "Correct: \(correctCollection.count)"
        completedLabel.text = "Completed: \(currentIndex) / \(cards.count)"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
