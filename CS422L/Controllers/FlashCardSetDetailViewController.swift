//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        makeItPretty()
    }
    
    @IBAction func exitStudy(unwindSegue: UIStoryboardSegue) {
    }
    
    // Send data to StudySetViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is StudySetViewController {
            let vc = segue.destination as? StudySetViewController
                vc?.cards = self.cards
        }
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcard()
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(item: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    @objc func handleLongPress(recognizer: UILongPressGestureRecognizer) {
        let tableCell = recognizer.view as! UITableViewCell
        let label = tableCell.contentView.subviews[0] as! UILabel
        let term = label.text
        
        var flashcard = Flashcard()
        for card in cards {
            if card.term == term {
                flashcard = card
                break
            }
        }
        
        createEditAlert(flashcard)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(recognizer:)))
                                                               
        cell.addGestureRecognizer(longPressRecognizer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
            Flashcard clicked
         */
        let cell = tableView.cellForRow(at: indexPath)
        let contentView = cell?.contentView
        let label = contentView?.subviews[0] as! UILabel
        let term = label.text
        
        var flashcard = Flashcard()
        for card in cards {
            if card.term == term {
                flashcard = card
            }
        }
        
        let alertController = UIAlertController(title: term, message:
                                                    flashcard.definition, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Back", style: .default, handler: {
            (alertAction: UIAlertAction) in
            print("Back Button Clicked")
        }))
        
        alertController.addAction(UIAlertAction(title: "Edit", style: .default, handler: {
            (alertAction: UIAlertAction) in
            self.createEditAlert(flashcard)
        }))

        self.present(alertController, animated: true, completion: nil)
    }
    
    func createEditAlert(_ flashcard: Flashcard) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: {
            (textField: UITextField) in
            textField.text = flashcard.term
        })
        
        alertController.addTextField(configurationHandler: {
            (textField: UITextField) in
            textField.text = flashcard.definition
        })
        
        let doneBtn = UIAlertAction(title: "Done", style: .default, handler: {
        (alert: UIAlertAction) in
            self.updateFlashcard(flashcard, alertController.textFields![0].text ?? flashcard.term, alertController.textFields![1].text ?? flashcard.definition)
        })
        
        alertController.addAction(doneBtn)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateFlashcard(_ flashcard: Flashcard, _ newTerm: String, _ newDefinition: String) {
        flashcard.term = newTerm
        flashcard.definition = newDefinition
        tableView.reloadData()
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
}
